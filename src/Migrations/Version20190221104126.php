<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190221104126 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE capteur ADD iot_id INT NOT NULL');
        $this->addSql('ALTER TABLE capteur ADD CONSTRAINT FK_5B4A1695F8063444 FOREIGN KEY (iot_id) REFERENCES iot (id)');
        $this->addSql('CREATE INDEX IDX_5B4A1695F8063444 ON capteur (iot_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE capteur DROP FOREIGN KEY FK_5B4A1695F8063444');
        $this->addSql('DROP INDEX IDX_5B4A1695F8063444 ON capteur');
        $this->addSql('ALTER TABLE capteur DROP iot_id');
    }
}
