<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190221095915 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE capteur ADD type_capteur_id INT NOT NULL');
        $this->addSql('ALTER TABLE capteur ADD CONSTRAINT FK_5B4A1695A0B740C FOREIGN KEY (type_capteur_id) REFERENCES type_capteur (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5B4A1695A0B740C ON capteur (type_capteur_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE capteur DROP FOREIGN KEY FK_5B4A1695A0B740C');
        $this->addSql('DROP INDEX UNIQ_5B4A1695A0B740C ON capteur');
        $this->addSql('ALTER TABLE capteur DROP type_capteur_id');
    }
}
