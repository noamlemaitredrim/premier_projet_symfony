<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CapteurRepository")
 */
class Capteur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeCapteur")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeCapteur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Iot")
     * @ORM\JoinColumn(nullable=false)
     */
    private $iot;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getTypeCapteur(): ?TypeCapteur
    {
        return $this->typeCapteur;
    }

    public function setTypeCapteur(TypeCapteur $typeCapteur): self
    {
        $this->typeCapteur = $typeCapteur;

        return $this;
    }

    public function getIot(): ?Iot
    {
        return $this->iot;
    }

    public function setIot(?Iot $iot): self
    {
        $this->iot = $iot;

        return $this;
    }
}
