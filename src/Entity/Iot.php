<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IotRepository")
 */
class Iot
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $ipLocale;

    /**
     * @var bool
     * @ORM\Column(name="actif",type="boolean")
     */
    private $actif;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Capteur", mappedBy="iot", orphanRemoval=true)
     */
    private $capteurs;

    public function __construct()
    {
        $this->capteurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getIpLocale(): ?string
    {
        return $this->ipLocale;
    }

    public function setIpLocale(?string $ipLocale): self
    {
        $this->ipLocale = $ipLocale;

        return $this;
    }

    public function getActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * @return Collection|Capteur[]
     */
    public function getCapteurs(): Collection
    {
        return $this->capteurs;
    }

    public function addCapteur(Capteur $capteur): self
    {
        if (!$this->capteurs->contains($capteur)) {
            $this->capteurs[] = $capteur;
            $capteur->setIot($this);
        }

        return $this;
    }

    public function removeCapteur(Capteur $capteur): self
    {
        if ($this->capteurs->contains($capteur)) {
            $this->capteurs->removeElement($capteur);
            // set the owning side to null (unless already changed)
            if ($capteur->getIot() === $this) {
                $capteur->setIot(null);
            }
        }

        return $this;
    }

    public function __toString() {
        $str = $this->nom.', '.$this->ipLocale;
        return $str;
    }
}