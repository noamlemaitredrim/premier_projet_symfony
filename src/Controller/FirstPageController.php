<?php
/**
 * Created by PhpStorm.
 * User: n.lemaitre
 * Date: 20/02/2019
 * Time: 08:35
 */

namespace App\Controller;

use App\Entity\Capteur;
use App\Entity\Iot;
use App\Entity\TypeCapteur;
use App\Form\CapteurType;
use App\Form\IotType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FirstPageController extends AbstractController
{

    public function HasardYml()
    {
        $number = random_int(0, 100);

        /*return new Response(
            '<html><body><p>FirstPage.HasardYml(): '.$number.'</p></body></html>'
        );*/

        return $this->render('firstpage/firstpage_hasardyml.html.twig',
            [
                'nomDemo' => 'hasardyml: route par fichier Yaml',
                'number' => $number,
                'message' => 'Le nombre est'
            ]);
    }

    /**
     * @Route("/firstpage/hasardannot/{nb}")
     */
    public function HasardAnnot($nb)
    {
        for ($i = 0; $i < $nb; $i++){
            $numbers[] = random_int(100, 200);
        }

        /*return new Response(
            '<html><body><p>FirstPage.HasardAnnot(): '.$number.'</p></body></html>'
        );*/

        return $this->render('firstpage/firstpage_hasardannot.html.twig',
            [
                'nomDemo' => 'hasardannot: route par le controller',
                'numbers' => $numbers,
                'message' => 'Le nombre est'
            ]);
    }

    /**
     * @Route("/firstpage/democreeriot")
     */
    public function DemoCreerIot()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $number = random_int(1, 100);
        $actif = (0 == $number%2) ? TRUE : FALSE;
        $iot = new Iot();

        $iot->setNom("DemoCreation_".$number);
        $iot->setIpLocale("127.0.0.".$number);
        $iot->setActif($actif);

        $entityManager->persist($iot);
        $entityManager->flush();

        return new Response(
            '<html><body><p>FirstPage.DemoCreerIot(): '.$iot->getId().'</p></body></html>'
        );
    }

    /**
     * @Route("/firstpage/demoshowiot/{id}",
     *     name="FirstPage_demoShowIot")
     */
    public function DemoShowIot($id)
    {
        $iot = $this->getDoctrine()
            ->getRepository(Iot::class)
            ->find($id);

        return new Response(
            '<html><body><p>FirstPage.DemoShowIot(): '.$iot->getNom().'</p></body></html>'
        );
    }

    /**
     * @Route("/firstpage/demoshowalliot", name="FirstPage_DemoShowAllIot")
     * Démo d'une méthode spécialisée du repository
     * @return Response la vue
     */
    public function DemoShowAllIot()
    {
        // Récupération des Iots modifiés en utilisant son repository et sa nouvelle méthode
        $iots = $this->getDoctrine()
            ->getRepository(Iot::class)
            ->findAll();

        //  Préparation de l'affichage
        $aff = '';
        /*foreach ($iots as $iot) {
            $aff .= 'Id: '.$iot->getId().
                ' Nom: '.$iot->getNom().
                ' IP: '.$iot->getIpLocale().
                ' Actif: '.$iot->getActif().
                '<br/>';
            //  Récupération et parcours des capteurs de l'iot
            $capteurs = $iot->getCapteurs();
            if(count($capteurs) > 0)
                $aff .= '>>> Capteurs:<br/>';
            foreach ($capteurs as $capteur)
            {
                $aff .= '-------> Nom: ' . $capteur->getNom().
                    ' type: ' . $capteur->getTypeCapteur()->getNom().
                    '<br/>';
            }
        }*/

        foreach ($iots as $iot) {
            $aff .= 'Id: '.$iot->getId().
                ' Nom: '.$iot->getNom().
                ' IP: '.$iot->getIpLocale().
                ' Actif: '.$iot->getActif().
                '<br/>';
            //  Récupération et parcours des capteurs de l'iot
            $capteurs = $iot->getCapteurs();
            if(count($capteurs) > 0)
                $aff .= '>>> Capteurs ('.$aff.'):<br/>';
            foreach ($capteurs as $capteur)
            {
                $aff .= $capteur->getNom().
                    ' type: ' . $capteur->getTypeCapteur()->getNom().
                    '<br/>';
            }
        }

        return new Response('<html><body><p>FirstPage.DemoShowAllIot():<br/>'.$aff.'</p></body></html>');
    }

    /**
     * @Route("/firstpage/demoupdateiot/{id}")
     */
    public function DemoUpdateIot($id)
    {
        $iot = $this->getDoctrine()
            ->getRepository(Iot::class)
            ->find($id);

        $iot->setNom("update");

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return $this->redirectToRoute(
            'FirstPage_demoShowIot',
            [
                'id' => $iot->getId()
            ]
        );
    }

    /**
     * @Route("/firstpage/demoshowallcapteurs")
     * Affichage de tous les capteurs
     * @return Response la vue
     */
    public function DemoShowAllCapteurs()
    {
        // Récupération des capteurs
        $capteurs = $this->getDoctrine()
            ->getRepository(Capteur::class)
            ->findAll();

        $aff = '';
        foreach ($capteurs as $capteur) {
            $aff .= $capteur->getId().' nom: '.$capteur->getNom().
//  Commenter les 3 lignes suivantes et observer le nombre de requête SQL
                ' iot: '.$capteur->getIot()->getNom().
                ' type: '.$capteur->getTypeCapteur()->getNom().
                ' unité: '.$capteur->getTypeCapteur()->getUnite().
                '<br/>';
        }

        return new Response('<html><body><p>FirstPage.DemoShowAllCapteurs():<br/>'.$aff.'</p></body></html>');
    }


    /**
     * @Route("/firstpage/demoshowmodifiediot")
     * Démo d'une méthode spécialisée du repository
     * @return Response la vue
     */
    public function DemoShowModifiedIot()
    {
        // Récupération des Iots modifiés en utilisant son repository et sa nouvelle méthode
        $iots = $this->getDoctrine()
            ->getRepository(Iot::class)
            ->findAllModifiedIot();

        $aff = 'Avec findAllModifiedIot():<br/>';
        foreach ($iots as $iot) {
            $aff .= 'Id: '.$iot->getId().' Nom: '.$iot->getNom().'<br/>';
        }

        // Récupération des Iots modifiés en utilisant son repository et sa nouvelle méthode
        $iots = $this->getDoctrine()
            ->getRepository(Iot::class)
            ->findAllModifiedIotSQL();

        $aff .= '<br/>Avec findAllModifiedIotSQL():<br/>';
        foreach ($iots as $iot) {
            $aff .= 'Id: '.$iot->getId().' Nom: '.$iot->getNom().'<br/>';
        }

        return new Response('<html><body><p>FirstPage.DemoShowModifiedIot():<br/>'.$aff.'</p></body></html>');
    }


    /**
     * @Route("/firstpage/democreercapteur")
     * Création d'un Capteur et persistance dans la bdd
     * @return Response la vue
     */
    public function DemoCreerCapteur()
    {
        // Récupération de l'EntityManager de Doctrine
        $entityManager = $this->getDoctrine()->getManager();

        //  Récupération du typeCapteur n°1
        $typeCapteur = $entityManager->getRepository(TypeCapteur::class)->find(1);
        //  Récupération de l'Iot n°5
        $iot = $entityManager->getRepository(Iot::class)->find(2);

        //  Instanciation du capteur et initialisation
        $number = random_int(1, 100);
        $capteur = new Capteur();
        $capteur->setNom("DemoCapteur_$number")
            ->setIot($iot)
            ->setTypeCapteur($typeCapteur);

        //  On persiste le capteur
        //  et on déclenche l’enregistrement dans la bdd
        $entityManager->persist($capteur);
        $entityManager->flush();

        //  Préparation affichage
        $aff = $capteur->getId().' nom: '.$capteur->getNom().
            //' iot: '.$capteur->getIot()->getNom().
            ' type: '.$capteur->getTypeCapteur()->getNom().
            ' unité: '.$capteur->getTypeCapteur()->getUnite().'<br/>';

        return new Response('<html><body><p>FirstPage.DemoCreerCapteur(): <br/>'.$aff.'</p></body></html>');
    }


    /**
     * @Route("/firstpage/demoaddiot")
     * Création d'un Iot avec un formulaire et persistance dans la bdd
     * @return Response la vue
     */
    public function DemoAddIot(Request $request)
    {
        //  Instanciation d'un Iot, avec des valeurs par défaut
        $iot = new Iot();
        $iot->setNom("DemoCreation");
        $iot->setActif(FALSE);

        //  Création du FormBuilder
        $formBuilder = $this->createFormBuilder($iot);

        //  Composition du formulaire à l'aide du FormBuilder
        //  Les attributs des inputs sont passés sous forme de tableau
        $formBuilder->add('nom', TextType::class,['required' => true])
            ->add('ipLocale', TextType::class)
            ->add('actif', ChoiceType::class, [
                'label' => 'Actif ?',
                'choices' => array('On'=> 1, 'Off' => 0,),
                'expanded' => true,
                'multiple' => false,
                'required' => true,
            ])
            ->add('save', SubmitType::class, ['label' => 'Enregistrer']);

        //  Récupère le formulaire généré, avec les valeurs de l'Iot instancié précédemment
        $form =	$formBuilder->getForm();

        //  Si on arrive en POST, c'est que le formulaire a été soumis.
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid())
        {
            //  L'iot est déjà hydraté avec le contenu du formulaire validé,
            //  on l'enregistre dans la bdd.
            $em = $this->getDoctrine()->getManager();
            $em->persist($iot);
            $em->flush();

            //  Redirection vers la route nommée FirstPage_DemoShowAllIot
            return $this->redirectToRoute('FirstPage_DemoShowAllIot');
        }

        return $this->render('firstpage/firstpage_demoaddiot_long.html.twig', [
            'nomDemo' => 'Ajouter un IoT',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/firstpage/demoeditiot/{id}")
     * Edition d'un Iot avec un formulaire et persistance dans la bdd
     * @return Response la vue
     */
    public function DemoEditIot(Request $request, $id)
    {
        $iot = $this->getDoctrine()
            ->getRepository(Iot::class)
            ->find($id);

        $form = $this->createForm(IotType::class, $iot);

        //  Si on arrive en POST, c'est que le formulaire a été soumis.
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid())
        {
            //  L'iot est déjà hydraté avec le contenu du formulaire validé,
            //  on l'enregistre dans la bdd.
            $em = $this->getDoctrine()->getManager();
            $em->persist($iot);
            $em->flush();

            //  Redirection vers la route nommée FirstPage_DemoShowAllIot
            return $this->redirectToRoute('FirstPage_DemoShowAllIot');
        }

        return $this->render('firstpage/firstpage_demoaddiot_long.html.twig', [
            'nomDemo' => 'Modifier un IoT',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/firstpage/demoeditcapteur/")
     * Edition d'un Capteur avec un formulaire et persistance dans la bdd
     * @return Response la vue
     */
    public function DemoAddCapteur(Request $request)
    {
        $capteur = new Capteur();

        $form = $this->createForm(CapteurType::class, $capteur);

        //  Si on arrive en POST, c'est que le formulaire a été soumis.
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid())
        {
            //  L'iot est déjà hydraté avec le contenu du formulaire validé,
            //  on l'enregistre dans la bdd.
            $em = $this->getDoctrine()->getManager();
            $em->persist($capteur);
            $em->flush();

            //  Redirection vers la route nommée FirstPage_DemoShowAllIot
            return $this->redirectToRoute('FirstPage_DemoShowAllIot');
        }

        return $this->render('firstpage/firstpage_demoaddcapteur.html.twig', [
            'nomDemo' => 'Modifier un Capteur',
            'form' => $form->createView(),
        ]);
    }


}