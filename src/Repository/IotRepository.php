<?php

namespace App\Repository;

use App\Entity\Iot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Iot|null find($id, $lockMode = null, $lockVersion = null)
 * @method Iot|null findOneBy(array $criteria, array $orderBy = null)
 * @method Iot[]    findAll()
 * @method Iot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IotRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Iot::class);
    }

    /**
     * Récupère tous les Iot dont le nom a été modifié (avec l'action update...)
     * @return array
     */
    public function findAllModifiedIot() : array
    {
        $qb = $this->createQueryBuilder('i')
            ->andWhere('i.nom = :nom')
            ->setParameter('nom', 'Nom_Modifié')
            ->orderBy('i.id', 'DESC')
            ->getQuery();

        return $qb->execute();
    }

    /**
     * Récupère tous les Iot dont le nom a été modifié (avec l'action update...)
     * Utilisation de SQL plutôt que du QueryBuilder.
     * @return array
     */
    public function findAllModifiedIotSQL() : array
    {
        //  Récupère l'entityManager
        $entityManager = $this->getEntityManager();

        //  Attention!! '*' cause un problème...
        $query = $entityManager->createQuery(
            'SELECT i FROM App\Entity\Iot i WHERE i.nom = :modif ORDER BY i.id DESC'
        )->setParameter('modif', 'Nom_Modifié');

        return $query->execute();
    }

    // /**
    //  * @return Iot[] Returns an array of Iot objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Iot
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}