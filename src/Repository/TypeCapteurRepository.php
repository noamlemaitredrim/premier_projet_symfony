<?php

namespace App\Repository;

use App\Entity\TypeCapteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TypeCapteur|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeCapteur|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeCapteur[]    findAll()
 * @method TypeCapteur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeCapteurRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TypeCapteur::class);
    }

    // /**
    //  * @return TypeCapteur[] Returns an array of TypeCapteur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeCapteur
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
